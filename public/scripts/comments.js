/**
 * Container for Comments
 */
var CommentBox = React.createClass({
    /**
     * Gets comments from server and updates the state of comments
     */
    loadCommentsFromServer: function() {
        $.ajax({
            url: this.props.url,
            dataType: 'json',
            cache: false,
            success: function(data) {
                this.setState({
                    data: data
                });
            }.bind(this),
            error: function(xhr, status, err) {
                console.error(this.props.url, status, err.toString());
            }.bind(this)
        });
    },

    /**
     * Invoked when Post button is clicked. Handles updating of new comment locally as well as persisting on server
     * @param  {Object} comment user entered comment
     */
    handleCommentSubmit: function(comment) {
        // update locally
        var comments = this.state.data;
        comment.id = Date.now();
        var newComments = comments.concat([comment]);
        this.setState({
            data: newComments
        });
        // persist on server
        $.ajax({
            url: this.props.url,
            dataType: 'json',
            type: 'POST',
            data: comment,
            success: function(data) {
                this.setState({
                    data: data
                });
            }.bind(this),
            error: function(xhr, status, err) {
                this.setState({
                    data: comments
                });
                console.error(this.props.url, status, err.toString());
            }
        });
    },

    getInitialState: function() {
        return {
            data: []
        };
    },

    componentDidMount: function() {
        this.loadCommentsFromServer();
        setInterval(this.loadCommentsFromServer, this.props.pollInterval);
    },

    render: function() {
        return (
            <div className="commentBox">
                <h1>Comments</h1>
                <CommentList data={this.state.data} />
                <CommentForm onCommentSubmit={this.handleCommentSubmit} />
            </div>
        );
    }
});

/**
 * Container for comments
 */
var CommentList = React.createClass({

    render: function() {
        var commentNodes = this.props.data.map(function(comment) {
            return (
                <Comment author={comment.author} key={comment.id}>{comment.text}</Comment>
            );
        });

        return (
            <div className="commentList">
                {commentNodes}
            </div>
        );
    }
});

/**
 * Represents individual comment. Responsible for rendering the comment passed in via props
 */
var Comment = React.createClass({
    /**
     * Sanitizes the comment text
     * @return {Object} sanitized, markedup html representation of text
     */
    rawMarkup: function() {
        var rawMarkup = marked(this.props.children.toString(), { sanitize: true });
        return { __html: rawMarkup };
    },

    render: function() {
        return (
            <div className="comment">
                <h2 className="commentAuthor">
                    {this.props.author}
                </h2>
                <span dangerouslySetInnerHTML={this.rawMarkup()}/>
            </div>
        );
    }
});

/**
 * Responsible for new comments.
 */
var CommentForm = React.createClass({

    getInitialState: function() {
        return {
            author: '',
            text: ''
        };
    },

    /**
     * Updates state representation ofinput field for author
     * @param  {Object} e change event
     */
    handleAuthorChange: function(e) {
        this.setState({
            author: e.target.value
        });
    },

    /**
     * Updates state representation ofinput field for text/ comment
     * @param  {Object} e change event
     */
    handleTextChange: function(e) {
        this.setState({
            text: e.target.value
        });
    },

    /**
     * Handles clicks on the Post button. Responsible for validating and saving new comments
     * @param  {Object} e change event
     */
    handleSubmit: function(e) {
        e.preventDefault();
        var author = this.state.author.trim();
        var text = this.state.text.trim();
        if (!text || !author) {
            return;
        }
        this.props.onCommentSubmit({
            author: author,
            text: text
        });
        this.setState({ author: '', text: '' });
    },

    render: function() {
        return (
            <form className="commentForm" onSubmit={this.handleSubmit}>
                <input type="text" placeholder="Your Name" value={this.state.author} onChange={this.handleAuthorChange}/>
                <input type="text" placeholder="Say Something..."  value={this.state.text} onChange={this.handleTextChange}/>
                <input type="submit" value="Post"/>
            </form>
        );
    }
});

// Fire up the system
ReactDOM.render(<CommentBox url="/api/comments" pollInterval={2000} />, document.getElementById('content'));
