# README #

Basic React tutorial from [Facebook](https://facebook.github.io/react/docs/tutorial.html)

### What is this repository for? ###

* Getting hands dirty with React
* Version 1.0.0

### How do I get set up? ###

* Clone this repository locally
* Open command prompt, navigate to the root folder of this project
* run `npm install`
* run `node server.js`
* Open the url (http://localhost:3000) in the browser